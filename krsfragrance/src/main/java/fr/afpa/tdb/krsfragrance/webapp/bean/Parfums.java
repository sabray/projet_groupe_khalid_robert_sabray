package fr.afpa.tdb.krsfragrance.webapp.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Parfums {
	@GeneratedValue
	@Id
	private Long id;
	private String nom;
	private String description;
	private String genre;
	private String notesOlfactives;
	private String ingredients;

	@ManyToOne
	private Marque marque;

	@OneToMany(mappedBy = "parfums")
	private List<Image> images;
	
	@ManyToMany(mappedBy = "parfumsFavoris")
	private List<Utilisateur> utilisateurs;

}
