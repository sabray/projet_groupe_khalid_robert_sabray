package fr.afpa.tdb.krsfragrance.webapp.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Utilisateur {

	@GeneratedValue
	@Id
	private Long id;
	private String nom;
	private String prenom;
	private String email;
	private String password;
	
	
	@ManyToMany
	private List<Parfums> parfumsFavoris;

}
