package fr.afpa.tdb.krsfragrance.webapp.bean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Marque {
	@GeneratedValue
	@Id
	private Long id;
	private String nom;
	private String description;

	@OneToMany(mappedBy = "marque")
	private List<Parfums> parfums;

}
