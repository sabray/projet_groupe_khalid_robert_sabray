package fr.afpa.tdb.krsfragrance.webapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.tdb.krsfragrance.webapp.bean.Marque;

@Repository
public interface MarqueDao extends JpaRepository<Marque, Long> {

}
