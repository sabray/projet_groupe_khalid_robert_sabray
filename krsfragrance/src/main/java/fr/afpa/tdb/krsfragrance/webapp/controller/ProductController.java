package fr.afpa.tdb.krsfragrance.webapp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.afpa.tdb.krsfragrance.webapp.bean.Product;

@CrossOrigin(origins = "http://localhost:8080", maxAge = 3600)
@Controller
public class ProductController {

	private static List<Product> produits = new ArrayList<>();
	private static Long compteurProduit = 0L;

	// ajoutProduit

	@RequestMapping(method = RequestMethod.GET, value = "index")
	public String affichageAjoutProduit() {

		return "index";
	}

	@RequestMapping(method = RequestMethod.GET, value = "accueil")
	public String affichageAjoutProduits() {

		return "accueil";
	}

	@RequestMapping(method = RequestMethod.GET, value = "ajouterProduit")
	public String affichageAjoutProdui() {

		return "ajouterProduit";
	}

	@RequestMapping(method = RequestMethod.GET, value = "produits")
	public String affichageAjouProduit() {

		return "Produits";
	}

	@RequestMapping(method = RequestMethod.GET, value = "hommes")
	public String affichagAjouthommes() {

		return "Hommes";
	}

	@RequestMapping(method = RequestMethod.GET, value = "femmes")
	public String affichagAJoutfemmes() {

		return "Femmes";
	}

	@CrossOrigin(origins = { "*", "all", "/**" })
	@RequestMapping(method = RequestMethod.GET, value = "marques")
	public String affichagAJoutmarques() {

		return "Marques";
	}

	@RequestMapping(method = RequestMethod.GET, value = "yvessaintlaurent")
	public String aFfichgAJoutyvessaintlaurent() {

		return "yvessaintlaurent";
	}

	@RequestMapping(method = RequestMethod.GET, value = "pacorabanne")
	public String aFfichgAJoutpacorabanne() {

		return "pacorabanne";
	}

	@RequestMapping(method = RequestMethod.GET, value = "chanel")
	public String aFfichgAJoutchanel() {
		return "chanel";
	}

	@RequestMapping(method = RequestMethod.GET, value = "dior")
	public String aFfichgAJoutdior() {
		return "dior";
	}

	@RequestMapping(method = RequestMethod.GET, value = "azzaro")
	public String aFfichgAJoutAzzaro() {
		return "azzaro";
	}

	@RequestMapping(method = RequestMethod.GET, value = "givenchy")
	public String aFfichgAJoutgivenchy() {
		return "givenchy";
	}

	@RequestMapping(method = RequestMethod.GET, value = "guerlain")
	public String aFfichgAJoutguerlain() {
		return "guerlain";
	}

}
