package fr.afpa.tdb.krsfragrance.webapp.controller;

import java.security.SecureRandom;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.afpa.tdb.krsfragrance.webapp.bean.Utilisateur;
import fr.afpa.tdb.krsfragrance.webapp.dao.UtilisateurDao;
import fr.afpa.tdb.krsfragrance.webapp.services.MailService;

@Controller
public class UtilisateurController {

	@Autowired
	UtilisateurDao utilisateurDao;
	@Autowired
	MailService mailService;

	@RequestMapping(method = RequestMethod.GET, value = "authentification")
	public String affichagAuthentification() {
		return "Authentification";
	}

	@RequestMapping(method = RequestMethod.POST, value = "do_authentification")
	public String doAuthentification(HttpSession session, Model model, String login, String password) {

		Utilisateur u = utilisateurDao.findByEmailAndPassword(login, password);
		if (u == null) {
			model.addAttribute("ErrorMessage", "Login/mdp invalids");
			return "Authentification";
		}
		session.setAttribute("connectedUser", u);
		return "redirect:accueil";
	}

	@RequestMapping(method = RequestMethod.GET, value = "creationCompte")
	public String affichagCreationCompte() {
		return "creationCompte";
	}

	@RequestMapping(method = RequestMethod.POST, value = "do_inscription")
	public String doCreationCompte(Utilisateur newUser) {
		utilisateurDao.save(newUser);
		return "redirect:authentification";
	}

	@GetMapping("afficher_mdp_perdu")
	public String afficherMdpPerdu() {
		return "afficher_mdp_perdu";
	}

	@PostMapping("do_changer_mdp")
	public String doChangerMdp(/* 1 */String email, Model model) {
		/* 2 */Utilisateur u = utilisateurDao.findByEmail(email);

		if (u == null) {
			model.addAttribute("ErrorMessage", "Le compte n'existe pas");
			return "afficher_mdp_perdu";
		}

		/* 3 */String nouveauPassword = generatePassayPassword(8);
		/* 4 */u.setPassword(nouveauPassword);
		/* 4 */utilisateurDao.save(u);
		/* 5 */String[] to = { u.getEmail() };
		String objet = "Nouveau mot de passe";
		String corps = "Bonjour " + u.getNom() + "\n";
		corps = corps + "Vous avez modifié votre mot de passe :  " + nouveauPassword + "  \n";
		corps = corps + "Cordialement,\n";
		corps = corps + "Votre site\n";
		String[] fichiers = { "C:\\Users\\saidm\\Desktop\\all.jpg",
				"C:\\Users\\saidm\\Desktop\\www.cours-gratuit.com--coursSpring-id4426.pdf" };
		/* 6 */mailService.sendEmailWithAttachment(to, objet, corps, fichiers);
		/* 7 */return "redirect:login";
	}

	public static String generatePassayPassword(int randomStrLength) {
		char[] possibleCharacters = (new String(
				"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?"))
						.toCharArray();
		String randomStr = RandomStringUtils.random(randomStrLength, 0, possibleCharacters.length - 1, false, false,
				possibleCharacters, new SecureRandom());
		return randomStr;
	}
}
