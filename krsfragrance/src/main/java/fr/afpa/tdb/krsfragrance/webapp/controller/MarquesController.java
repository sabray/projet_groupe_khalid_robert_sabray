package fr.afpa.tdb.krsfragrance.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import fr.afpa.tdb.krsfragrance.webapp.bean.Marque;
import fr.afpa.tdb.krsfragrance.webapp.dao.MarqueDao;

@Controller
public class MarquesController {

	@Autowired
	MarqueDao marqueDao;
	
	@GetMapping(value = "afficher_detail_marque")
	public String afficherDetailsMarque(Model model , Long idMarque) {
		Marque maMarque = marqueDao.getOne(idMarque);
		model.addAttribute("marqueIhm", maMarque);
		return "administration/marques/details_marque";
	}
	
	@GetMapping(value = "editer_marque")
	public String editerDetailsMarque(Model model , Long idMarque) {
		Marque maMarque = marqueDao.getOne(idMarque);
		model.addAttribute("marqueIhm", maMarque);
		model.addAttribute("edit", true);
		return "administration/marques/details_marque";
	}
	
	
	@PostMapping(value = "do_editer_marque")
	public String doEditerDetailsMarque(Model model , Marque marque) {
		marqueDao.save(marque);
		
		return "redirect:liste_marques";
	}
	
	
	@GetMapping(value = "ajouter_marque")
	public String afficherAjouterMarque() {
		return "administration/marques/ajouter_marque";
	}
	
	@PostMapping(value = "do_ajouter_marque")
	public String faireAjouterMarque(Marque marque) {
		marqueDao.save(marque);
		return "redirect:liste_marques";
	}
	
	@GetMapping(value = "liste_marques")
	public String afficherListeMarque(Model model) {
		model.addAttribute("marquesIhm", marqueDao.findAll());
		return "administration/marques/liste_marques";
	}
	
	
	@GetMapping(value = "supprimer_marque")
	public String supprimerDetailsMarque(Long idMarque) {
		marqueDao.deleteById(idMarque);
		return "redirect:liste_marques";
	}
	
	
}
