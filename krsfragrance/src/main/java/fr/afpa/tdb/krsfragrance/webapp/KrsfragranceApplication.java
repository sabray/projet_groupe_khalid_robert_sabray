package fr.afpa.tdb.krsfragrance.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KrsfragranceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KrsfragranceApplication.class, args);
	}

}
