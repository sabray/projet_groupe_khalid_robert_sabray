package fr.afpa.tdb.krsfragrance.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import fr.afpa.tdb.krsfragrance.webapp.bean.Parfums;
import fr.afpa.tdb.krsfragrance.webapp.dao.ParfumsDao;

@Controller
public class ParfumsController {

	@Autowired
	ParfumsDao parfumsDao;

	@GetMapping(value = "details_parfums")
	public String afficherDetailsParfums() {
		return "details_parfums";
	}

	@GetMapping(value = "ajouter_parfums")
	public String afficherAjouterParfums() {
		return "administration/parfums/ajouter_parfums";
	}

	@PostMapping(value = "do_ajouter_parfums")
	public String faireAjouterparfums(Parfums parfums) {
		parfumsDao.save(parfums);
		return "redirect:liste_parfums";
	}

	@GetMapping(value = "liste_parfums")
	public String afficherListeparfums(Model model) {
		model.addAttribute("parfumsIhm", parfumsDao.findAll());
		return "administration/parfums/liste_parfums";
	}

}
