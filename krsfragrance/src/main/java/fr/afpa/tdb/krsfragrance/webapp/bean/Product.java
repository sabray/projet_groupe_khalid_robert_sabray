package fr.afpa.tdb.krsfragrance.webapp.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Product {
	@Id
	@GeneratedValue
	private Long id;

	private String nom;
	private String description;
	private Double prix;
	private String url;
	private String model;
	private String marque;

	public Product() {

	}

	public Product(Long id, String nom, String description, Double prix, String url, String couleur, String model,
			String marque) {
		super();
		this.nom = nom;
		this.description = description;
		this.prix = prix;
		this.url = url;
		this.model = model;
		this.marque = marque;

	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrix() {
		return prix;
	}

	public void setPrix(Double prix) {
		this.prix = prix;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
}
