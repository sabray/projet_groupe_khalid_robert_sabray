package fr.afpa.tdb.krsfragrance.webapp.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.tdb.krsfragrance.webapp.bean.Utilisateur;

@Repository
public interface UtilisateurDao extends JpaRepository<Utilisateur, Long> {

	public Utilisateur findByEmailAndPassword(String email, String password);

	public Utilisateur findByEmail(String email);
}